//
//  CategoriesVC.swift
//  MedDream
//
//  Created by user1 on 19/07/21.
//

import UIKit

class CategoriesVC: UIViewController {
    
    @IBOutlet var BGImage : UIImageView!
    @IBOutlet var imgPoster : UIImageView!

    @IBOutlet var viewScroll : UIView!
    @IBOutlet var viewFirstPoster : UIView!
    @IBOutlet var viewAds: UIView!
    @IBOutlet var viewSegment : UIView!
    @IBOutlet var viewShadow : UIView!
    
    @IBOutlet var collectionCat : UICollectionView!
    @IBOutlet var collectionCat_Option : UICollectionView!
    
    @IBOutlet var collectionHeight : NSLayoutConstraint!
        
    var arrCatName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BuildUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        DispatchQueue.main.async {
            self.collectionCat_Option.collectionViewLayout.invalidateLayout()
        }
    }

    func BuildUI() {
                
        BGImage.frame = self.view.bounds
        view.insertSubview(BGImage, at: 0)
        view.insertSubview(viewScroll, at: 1)
        
        viewFirstPoster.layer.cornerRadius = 15.0
        viewAds.layer.cornerRadius = 15.0
        imgPoster.layer.cornerRadius = 15.0
        viewShadow.roundCorners([.bottomLeft , .bottomRight], radius: 15.0)

        arrCatName = ["All","Sleep stories","Meditation","Music","Nature","Background noise"]
        
        collectionCat_Option.reloadData()
        collectionCat.reloadData()
    }

}

extension CategoriesVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionCat {
            return CGSize(width: collectionCat.frame.size.width / 2 - 10 , height: 200)
        } else {
            
            let text = arrCatName[indexPath.row]
            let width = self.estimatedFrame(text: text, font: UIFont.systemFont(ofSize: 16)).width
            
            return CGSize(width: width + 60 , height: 50)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionCat {
            let arrCount = 20
            let height = CGFloat(round(CGFloat(arrCount)) / 2 * 210)
            collectionHeight.constant = height
            return 20
        } else {
            return arrCatName.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionCat {
            let cell = collectionCat.dequeueReusableCell(withReuseIdentifier: "cell_TopRated", for: indexPath) as! cell_TopRated
            cell.cellBuildUI()
            return cell
        } else {
            let cell = collectionCat_Option.dequeueReusableCell(withReuseIdentifier: "cell_Cat_Option", for: indexPath) as! cell_Cat_Option
            cell.cellBuildUI()
            cell.awakeFromNib()
            cell.lblCatName.text = arrCatName[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionCat {
            DispatchQueue.main.async(execute: {
                self.tabBarController?.selectedIndex = 6
                Fire_Simple_Notification(strNotificationName: Ring_Image_Hide_Notifiation)
            })
        } else {
        }
    }
}
