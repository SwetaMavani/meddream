//
//  PresentLayoutVC.swift
//  MedDream
//
//  Created by user1 on 20/07/21.
//

import UIKit

class PresentLayoutVC: UIViewController {

    @IBOutlet var collectionCat : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.moreNavigationController.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionCat.reloadData()
    }
}

extension PresentLayoutVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 1 {
            return CGSize(width: collectionCat.frame.size.width , height: 100)
        } else {
            return CGSize(width: collectionCat.frame.size.width / 2 - 10 , height: 200)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if section == 0 {
            return 4
        } else if section == 1{
            return 1
        } else {
            return 20
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {

            let cell = collectionCat.dequeueReusableCell(withReuseIdentifier: "cell_TopRated", for: indexPath) as! cell_TopRated
            cell.cellBuildUI()
            return cell
        } else if indexPath.section == 1 {
            
            let cell = collectionCat.dequeueReusableCell(withReuseIdentifier: "cell_Ads", for: indexPath) as! cell_Ads
            cell.cellBuildUI()
            return cell
        } else {
            let cell = collectionCat.dequeueReusableCell(withReuseIdentifier: "cell_TopRated_Two", for: indexPath) as! cell_TopRated
            cell.cellBuildUI()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 1 {
            DispatchQueue.main.async(execute: {
                self.tabBarController?.selectedIndex = 3
                let dict : [String : String] = ["gif_name": "butterfly.gif"]
                GIF_NSNotificationFire(dict: dict)
            })
        } 
    }
}
