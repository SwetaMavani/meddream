//
//  MusicPlayVC.swift
//  MedDream
//
//  Created by user1 on 21/07/21.
//

import UIKit
import SwiftyGif

class MusicPlayVC: UIViewController {

    @IBOutlet var imgGIF : UIImageView!
    
    @IBOutlet var viewScroll : UIView!
    
    @IBOutlet var playerSlider : UISlider!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.moreNavigationController.navigationBar.isHidden = true
        self.BuildUI()
    }
    
    func BuildUI() {

        UITabBar.appearance().barTintColor = UIColor.clear
        DispatchQueue.main.async(execute: {
            do {
                let gif = try UIImage(gifName: "music_background_gif.gif")
                self.imgGIF.setGifImage(gif, loopCount: -1)
                self.imgGIF.contentMode = .scaleToFill
                self.view.insertSubview(self.imgGIF, at: 0)
                self.view.insertSubview(self.viewScroll, at: 1)
            } catch {
                print(error)
            }
        })
        self.setSlider(slider: self.playerSlider)
        self.playerSlider.minimumValue = 0
        self.playerSlider.value = 0.5
        self.playerSlider.maximumValue = 1
        
    }

    func setSlider(slider:UISlider) {
        
        let tgl = CAGradientLayer()
        let frame = CGRect.init(x:0, y:0, width:slider.frame.size.width, height:5)
        tgl.frame = frame
        tgl.colors = [UIColor.ThemeBlueColor.cgColor, UIColor.ThemePurpleColor.cgColor]
        tgl.startPoint = CGPoint.init(x:0.0, y:0.5)
        tgl.endPoint = CGPoint.init(x:1.0, y:0.5)
        
        UIGraphicsBeginImageContextWithOptions(tgl.frame.size, tgl.isOpaque, 0.0);
        tgl.render(in: UIGraphicsGetCurrentContext()!)
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            
            image.resizableImage(withCapInsets: UIEdgeInsets.zero)
            
            slider.setMinimumTrackImage(image, for: .normal)
        }
    }

    @IBAction func timeSliderChangeValue(_ sender: UISlider) {
        
        playerSlider.value = sender.value
    }

    @IBAction func btnDownPressed(_ sender : UIButton) {
        
        self.tabBarController?.selectedIndex = 0
        Fire_Simple_Notification(strNotificationName: Show_Music_view_Notifiation)
    }
    
    @IBAction func btnVolumePressed(_ sender : UIButton) {
        
    }
    
    @IBAction func btnBackWardPressed(_ sender : UIButton) {
        
    }
    
    @IBAction func btnForkWardPressed(_ sender : UIButton) {
        
    }

    @IBAction func btnPlay_PausePressed(_ sender : UIButton) {
        
    }

    @IBAction func btnRepeatPressed(_ sender : UIButton) {
        
    }

    @IBAction func btnRandomPressed(_ sender : UIButton) {
        
    }
}

