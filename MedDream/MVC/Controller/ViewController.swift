//
//  ViewController.swift
//  MedDream
//
//  Created by Mavani on 17/07/21.
//

import UIKit
import SwiftyGif

class ViewController: UIViewController {

    @IBOutlet var viewScroll : UIView!
    @IBOutlet var view_BG_Shadow : UIView!
    @IBOutlet var viewFirstPoster : UIView!
    @IBOutlet var viewSecondPoster : UIView!
    @IBOutlet var viewTopRated : UIView!
    @IBOutlet var viewAds: UIView!
    @IBOutlet var viewMostPopular : UIView!
    @IBOutlet var viewListingNow : UIView!
    @IBOutlet var viewShadow : UIView!
    @IBOutlet var viewScrollShadow : UIView!

    @IBOutlet var imgPoster : UIImageView!
    @IBOutlet var imgGIF : UIImageView!
    
    @IBOutlet var collectionTopRated : UICollectionView!
    @IBOutlet var collectionListing : UICollectionView!
    @IBOutlet var collectioMostPopular : UICollectionView!
    
    @IBOutlet var btnSetting : UIButton!
    
    @IBOutlet var lblPoster1_Title : UILabel!
    @IBOutlet var lblPoster1_Desc : UILabel!
    @IBOutlet var lblPoster2_Title : UILabel!
    @IBOutlet var lblPoster2_Desc : UILabel!
    @IBOutlet var lblTopRated : UILabel!
    @IBOutlet var lblMostPopular : UILabel!
    @IBOutlet var lblListingNow : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.BuildUI()
    }
    
    func BuildUI() {

        UITabBar.appearance().barTintColor = UIColor.clear
        
        DispatchQueue.main.async(execute: {
            do {
                let gif = try UIImage(gifName: "mainscreen.gif")
                self.imgGIF.setGifImage(gif, loopCount: -1)
                self.imgGIF.contentMode = .scaleToFill
                self.view.insertSubview(self.imgGIF, at: 0)
                self.view.insertSubview(self.view_BG_Shadow, at: 1)
                self.view.insertSubview(self.viewScroll, at: 2)
            } catch {
                print(error)
            }
            
            let gradient = CAGradientLayer()
            gradient.frame = self.view.bounds
            gradient.colors = [UIColor.ThemeDarkBlueAlphaColor.cgColor, UIColor.ThemeDarkBlueColor.cgColor]
            self.view_BG_Shadow.layer.insertSublayer(gradient, at: 0)
            
            let gradienttt = CAGradientLayer()
            gradienttt.frame = self.viewScrollShadow.bounds
            gradienttt.colors = [UIColor.ThemeDarkBlueAlphaColor.cgColor, UIColor.ThemeDarkBlueColor.cgColor]
            self.viewScrollShadow.layer.insertSublayer(gradienttt, at: 0)

            self.viewFirstPoster.layer.cornerRadius = 15.0
            self.viewSecondPoster.layer.cornerRadius = 15.0
            self.viewTopRated.layer.cornerRadius = 15.0
            self.viewAds.layer.cornerRadius = 15.0
            self.viewMostPopular.layer.cornerRadius = 15.0
            self.viewListingNow.layer.cornerRadius = 15.0
            self.viewShadow.roundCorners([.bottomLeft , .bottomRight], radius: 15.0)

            self.imgPoster.layer.cornerRadius = 15.0
            
            self.collectionTopRated.reloadData()
            self.collectionListing.reloadData()
            self.collectioMostPopular.reloadData()
        })
        
    }
    
    //MARK:-Actions
    @IBAction func btnSettingPressed(_ sender : UIButton) {
        
    }
}


extension ViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionTopRated {
            let cell = collectionTopRated.dequeueReusableCell(withReuseIdentifier: "cell_TopRated", for: indexPath) as! cell_TopRated
            cell.cellBuildUI()
            return cell
        } else if collectionView == collectionListing{
            let cell = collectionListing.dequeueReusableCell(withReuseIdentifier: "cell_TopRated", for: indexPath) as! cell_TopRated
            cell.cellBuildUI()
            return cell
        } else {
            let cell = collectioMostPopular.dequeueReusableCell(withReuseIdentifier: "cell_Most_Popular", for: indexPath) as! cell_Most_Popular
            cell.cellBuildUI()
            return cell
        }
    }
}
