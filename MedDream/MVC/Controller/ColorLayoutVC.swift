//
//  ColorLayoutVC.swift
//  MedDream
//
//  Created by user1 on 20/07/21.
//

import UIKit
import FlexColorPicker

open class ColorLayoutVC : UIViewController {
    
    @IBOutlet var viewCancel : UIView!
    @IBOutlet var viewAccept : UIView!
    
    let colorPicker = ColorPickerController()

    open override func viewDidLoad() {
        // 0 91 125
        super.viewDidLoad()
        tabBarController?.moreNavigationController.navigationBar.isHidden = true
        
        viewAccept.dropShadow()
        viewCancel.dropShadow()
    }
    
    @IBAction func btnAcceptPressed(_ sender : UIButton) {
        
    }
    
    @IBAction func btnCancelPressed(_ sender : UIButton) {
        
    }

}

extension ColorLayoutVC : ColorPickerControllerProtocol {
   

    open var delegate: ColorPickerDelegate? {
        get {
            return colorPicker.delegate
        }
        set {
            colorPicker.delegate = newValue
        }
    }

    /// Color currently selected by color picker.
    @IBInspectable
    open var selectedColor: UIColor {
        get {
            return colorPicker.selectedColor
        }
        set {
            colorPicker.selectedColor = newValue
        }
    }

    @IBOutlet public var colorPreview: ColorPreviewWithHex? {
        get {
            return colorPicker.colorPreview
        }
        set {
            colorPicker.colorPreview = newValue
        }
    }

    @IBOutlet open var radialHsbPalette: RadialPaletteControl? {
        get {
            return colorPicker.radialHsbPalette
        }
        set {
            colorPicker.radialHsbPalette = newValue
        }
    }

    @IBOutlet open var saturationSlider: SaturationSliderControl? {
        get {
            return colorPicker.saturationSlider
        }
        set {
            colorPicker.saturationSlider = newValue
        }
    }

    @IBOutlet open var brightnessSlider: BrightnessSliderControl? {
        get {
            return colorPicker.brightnessSlider
        }
        set {
            colorPicker.brightnessSlider = newValue
        }
    }

}
