//
//  PreferredStatusBarStyleTabBarViewController.swift
//  MedDream
//
//  Created by Mavani on 17/07/21.
//

import Foundation
import UIKit

class PreferredStatusBarStyleTabBarViewController: UITabBarController {
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let selectedViewController = selectedViewController {
            return selectedViewController.preferredStatusBarStyle
        }
        
        return super.preferredStatusBarStyle
    }
    
}

class PreferredStatusBarStyleNavigationController: UINavigationController {
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let selectedViewController = topViewController {
            
            return selectedViewController.preferredStatusBarStyle
        }
        
        return super.preferredStatusBarStyle
    }
}
