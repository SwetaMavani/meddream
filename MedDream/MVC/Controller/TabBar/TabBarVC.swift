//
//  TabBarVC.swift
//  MedDream
//
//  Created by Mavani on 17/07/21.
//

import UIKit

class TabBarVC : UIViewController {

    @IBOutlet var imgHomeRing : UIImageView!
    @IBOutlet var imgMusicRing : UIImageView!
    
    @IBOutlet var viewSubButtons : UIView!
    @IBOutlet var viewMusic : UIView!

    @IBOutlet var Constraint_Y_SubButtons : NSLayoutConstraint!

    //TabBar
    var embeddedTabBarController = UITabBarController()
    var selectedTab = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSelectedIndex(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewMusic.isHidden = true

        NotificationCenter.default.addObserver(self, selector: #selector(setGIF(notification:)), name: NSNotification.Name(rawValue: GIF_Notification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.BottomViewShow(notification:)), name: Notification.Name(Show_Music_view_Notifiation), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HideRingImage(notification:)), name: Notification.Name(Ring_Image_Hide_Notifiation), object: nil)

    }
    
    @objc func setGIF(notification: NSNotification) {
        
        imgHomeRing.isHidden = true
        imgMusicRing.isHidden = true
        HideShowView(Y: 500, isHide: true)
    }

    @objc func BottomViewShow(notification: Notification) {
        viewMusic.isHidden = false
        self.imgHomeRing.isHidden = false
        self.imgMusicRing.isHidden = true
    }
    
    @objc func HideRingImage(notification: NSNotification) {
        self.imgMusicRing.isHidden = true
        self.imgHomeRing.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        HideShowView(Y: 500, isHide: true)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {

        guard embeddedTabBarController != nil else { return .default }
        return embeddedTabBarController.preferredStatusBarStyle
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedTabBarController" {
            embeddedTabBarController = segue.destination as! UITabBarController
        }
    }
    
    func HideShowView(Y : CGFloat , isHide : Bool) {
        
        self.Constraint_Y_SubButtons.constant = Y
        self.viewSubButtons.isHidden = isHide
        self.view.setNeedsLayout()
    }
         
    //MARK:- funtions
    func setSelectedIndex(_ index: Int) {
        
        imgHomeRing.isHidden = true
        imgMusicRing.isHidden = true
        
        HideShowView(Y: 500, isHide: true)
        
        DispatchQueue.main.async(execute: {
        
            if self.viewMusic.isHidden == true {
                if index == 0 {
                    self.imgHomeRing.isHidden = false
                    self.embeddedTabBarController.selectedIndex = index
                } else if index == 1{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self.HideShowView(Y: 0, isHide: false)
                    })
                } else if index == 2{
                    self.imgMusicRing.isHidden = false
                    self.embeddedTabBarController.selectedIndex = index
                } else if index == 3 {
                    self.embeddedTabBarController.selectedIndex = index
                    let dict : [String: String] = ["gif_name": "rgb.gif"]
                    GIF_NSNotificationFire(dict: dict)
                } else if index == 4 {
                    self.embeddedTabBarController.selectedIndex = index
                }else if index == 5 {
                    self.embeddedTabBarController.selectedIndex = index
                }
            } else {
                self.imgHomeRing.isHidden = false
            }
        })
    }

    //MARK:- Actions
    @IBAction func btnTabsPressed(_ sender : UIButton) {
        
        setSelectedIndex(sender.tag)
    }

    @IBAction func btnMusicPressed(_ sender : UIButton) {
        
        viewMusic.isHidden = true
        imgHomeRing.isHidden = true
        imgMusicRing.isHidden = true
        self.embeddedTabBarController.selectedIndex = 6
    }
}
