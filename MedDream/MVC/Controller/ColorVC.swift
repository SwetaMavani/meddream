//
//  ColorVC.swift
//  MedDream
//
//  Created by user1 on 20/07/21.
//

import UIKit

class ColorVC: UIViewController {

    @IBOutlet var viewScroll : UIView!
    
    @IBOutlet var imgGIF : UIImageView!
    
    var gifName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setGIF(notification:)), name: NSNotification.Name(rawValue: GIF_Notification), object: nil)
    }
    
    @objc func setGIF(notification: NSNotification) {
        if let gif = notification.userInfo?["gif_name"] as? String {
            BuildUI(strGIF: gif)
        }
    }

    func BuildUI(strGIF : String) {
        UITabBar.appearance().barTintColor = UIColor.clear
        DispatchQueue.main.async(execute: {
            do {
                let gif = try UIImage(gifName : strGIF)
                self.imgGIF.setGifImage(gif, loopCount: -1)
                self.imgGIF.contentMode = .scaleToFill
                self.view.insertSubview(self.imgGIF, at: 0)
                self.view.insertSubview(self.viewScroll, at: 1)
            } catch {
                print(error)
            }
        })
    }
}

