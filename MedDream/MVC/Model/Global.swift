//
//  Global.swift
//  MedDream
//
//  Created by user1 on 19/07/21.
//

import Foundation
import UIKit

let screenSize = UIScreen.main.bounds
let ScreenWidth = screenSize.width
let screenHeight = screenSize.height

let GIF_Notification = "GIF_Notification"
let Show_Music_view_Notifiation = "Show_Music_view_Notifiation"
let Ring_Image_Hide_Notifiation = "Ring_Image_Hide_Notifiation"

let cinzeldecorative_black = "cinzeldecorative_black"
let cinzeldecorative_bold = "cinzeldecorative_bold"
let cinzeldecorative_regular  = "cinzeldecorative_regular"

let helvetica_bold = "helvetica_bold"
let helvetica_bolditalic = "helvetica_bolditalic"
let helvetica_italic = "helvetica_italic"
let helvetica_light = "helvetica_light"
let helvetica_regular = "helvetica_regular"

func GIF_NSNotificationFire(dict : [String : String]) {

    NotificationCenter.default.post(name: NSNotification.Name(rawValue: GIF_Notification) , object: nil, userInfo: dict)
}

func Fire_Simple_Notification(strNotificationName : String) {
    
    NotificationCenter.default.post(name: Notification.Name(strNotificationName), object: nil)
}

//Remove Both side color from slider while remove Thumnail.
class CenteredThumbSlider : UISlider {
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        
        let unadjustedThumbrect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
    let thumbOffsetToApplyOnEachSide:CGFloat = unadjustedThumbrect.size.width / 2.0
    let minOffsetToAdd = -thumbOffsetToApplyOnEachSide
    let maxOffsetToAdd = thumbOffsetToApplyOnEachSide
    let offsetForValue = minOffsetToAdd + (maxOffsetToAdd - minOffsetToAdd) * CGFloat(value / (self.maximumValue - self.minimumValue))
    var origin = unadjustedThumbrect.origin
    origin.x += offsetForValue
    return CGRect(origin: origin, size: unadjustedThumbrect.size)
  }
}
