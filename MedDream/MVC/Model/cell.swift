//
//  cell.swift
//  MedDream
//
//  Created by Mavani on 19/07/21.
//

import Foundation
import UIKit

class cell_TopRated : UICollectionViewCell {

    @IBOutlet var imgCat : UIImageView!
    
    @IBOutlet var viewMain : UIView!
    
    @IBOutlet var lblCatName : UILabel!
    
    func cellBuildUI()  {
        
        imgCat.layer.cornerRadius = 15.0
        viewMain.layer.cornerRadius = 15.0
    }
}

class cell_Most_Popular : UICollectionViewCell {

    @IBOutlet var imgCat : UIImageView!
    
    @IBOutlet var viewMain : UIView!
    
    func cellBuildUI()  {
        
        imgCat.layer.cornerRadius = 15.0
        viewMain.layer.cornerRadius = 15.0
    }
}

class cell_Ads: UICollectionViewCell {
    
    @IBOutlet var viewAds : UIView!
    
    func cellBuildUI()  {
        
        viewAds.layer.cornerRadius = 15.0
    }
}

class cell_Cat_Option : UICollectionViewCell {

    @IBOutlet var imgCat : UIImageView!
    
    @IBOutlet var viewRing : UIView!
    @IBOutlet var viewMain : UIView!
    
    @IBOutlet var lblCatName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame = self.viewRing.bounds
            gradientLayer.colors =
                [UIColor.ThemeBlueColor.cgColor,UIColor.ThemePurpleColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradientLayer.cornerRadius = self.viewRing.frame.size.height / 2
            self.viewRing.layer.addSublayer(gradientLayer)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
//        DispatchQueue.main.async {
//            let gradientLayer:CAGradientLayer = CAGradientLayer()
//            gradientLayer.frame = self.viewRing.bounds
//            gradientLayer.colors =
//            [UIColor.ThemeBlueColor.cgColor,UIColor.ThemePurpleColor.cgColor]
//            gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
//            gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//            gradientLayer.cornerRadius = self.viewRing.frame.size.height / 2
//            self.viewRing.layer.addSublayer(gradientLayer)
//        }
    }
        
    func cellBuildUI()  {
        viewMain.layer.cornerRadius = viewMain.frame.size.height / 2
        viewRing.layer.cornerRadius = viewRing.frame.size.height / 2
    }
}
