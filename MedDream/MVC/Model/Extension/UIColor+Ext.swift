//
//  UIColor+Ext.swift
//  MedDream
//
//  Created by Mavani on 21/07/21.
//

import Foundation
import UIKit

extension UIColor {
    
    static let ThemeBlueColor = UIColor(red: 21.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0)

    static let ThemePurpleColor = UIColor(red: 129.0/255.0, green: 21.0/255.0, blue: 214.0/255.0, alpha: 1.0)
    
    static let ThemeDarkBlueColor = UIColor(red: 3.0/255.0, green: 23.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    
    static let ThemeDarkBlueAlphaColor = UIColor(red: 3.0/255.0, green: 23.0/255.0, blue: 76.0/255.0, alpha: 0.5)

    static let ThemePosterTextColor = UIColor(red: 110.0/255.0, green: 134.0/255.0, blue: 198.0/255.0, alpha: 1.0)

}

