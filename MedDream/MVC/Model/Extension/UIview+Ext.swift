//
//  UIview+Ext.swift
//  MedDream
//
//  Created by Mavani on 19/07/21.
//

import Foundation
import UIKit

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
  }
    
    func addGradient(with layer: CAGradientLayer, gradientFrame: CGRect? = nil, colorSet: [UIColor],
                     locations: [Double], startEndPoints: (CGPoint, CGPoint)? = nil) {
        layer.frame = gradientFrame ?? self.bounds
        layer.frame.origin = .zero

        let layerColorSet = colorSet.map { $0.cgColor }
        let layerLocations = locations.map { $0 as NSNumber }

        layer.colors = layerColorSet
        layer.locations = layerLocations

        if let startEndPoints = startEndPoints {
            layer.startPoint = startEndPoints.0
            layer.endPoint = startEndPoints.1
        }

        self.layer.insertSublayer(layer, above: self.layer)
    }

    func dropShadow() {
        
        layer.cornerRadius = 25
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.ThemeDarkBlueColor.cgColor
        layer.shadowColor = UIColor.ThemeDarkBlueColor.cgColor
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 4.0
    }

}


extension UILabel {
    
    func setLabelFont(fontName : String , fontSize : CGFloat, color : UIColor) {
        self.font = UIFont(name: fontName, size: fontSize)
        self.textColor = color
    }
}

extension UIButton {
    
    func setButtonFont(fontName : String , fontSize : CGFloat ,  color : UIColor) {
        self.titleLabel?.font = UIFont(name: fontName, size: fontSize)
        self.setTitleColor(color, for: .normal)
    }
}
